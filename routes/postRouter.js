const express = require("express");
const router = express.Router();
const Todo = require("../models/postModel");

router.get("/new",(req,res)=> {
    res.render("post/newPost",{user:req.user})
})

router.post("/new",(req,res)=> {
    var todo = new Todo({
        title: req.body.title,
        todo: req.body.todo,
        postNick:req.body.postNick
    })
    todo.save().then(results => {
        console.log(results);
        res.redirect("/");
    }).catch(err=> {
        console.log(err);
        res.redirect("/");
    })
})


module.exports = router;