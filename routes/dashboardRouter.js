const express = require("express");
const Entry = require("../models/entryModel");
const User = require("../models/userModel");
const path = require("path")

const router = express.Router();

router.get("/dashboard", (req, res) => {
  const {nick} = req.user;
  console.log("dash",nick);
  Entry.find({postNick:nick}).then((result,err)=> {
    if (err) {
      console.log(err);
    }else{
      User.find({nick:nick}).then(result2=> {
        console.log("dashEntry:",result);
        res.render("dashboard/dashboard", {user:nick,entry:result.reverse(),userPic:result2})
      })
    }
  })
});
router.post("/dashboard", (req, res) => {
  const user = req.user.nick;
  const file = req.files.profileImg;
  file.mv(path.resolve(__dirname,"../public/img/profile-img",file.name))
  console.log("img",req.files.profileImg.name);
  User.findOneAndUpdate({nick:user},{profileImg:`../../public/img/profile-img/${file.name}`}).then(result=> {
    res.redirect("/user/dashboard")
  })
 
});

module.exports = router;
