const express = require("express");
const router = express.Router();
const Entry = require("../models/entryModel");
const User = require("../models/userModel");

User.find({}).then(result=> {
    //console.log("user page",result);
    result.forEach(item=> {
        router.get(`/${item.nick.replace(/\s/g ,"-")}`,(req,res)=> {
            const nick = item.nick;
            console.log("user public:",nick);
            Entry.find({postNick:nick}).then((result,err)=> {
                if (err) {
                    console.log(err);
                }else{
                    User.find({nick:nick}).then(result2=> {
                        console.log("user public:",result);
                        res.render("user/public", {user:nick,entry:result.reverse(),userPic:result2})
                    })
                }
            })       
        })
    })
}).catch(err=> {
    console.log(err);
})



module.exports = router;