const express = require("express");

const router = express.Router();
const Entry = require("../models/entryModel");
const Title = require("../models/titleModel");

router.get("/edit/:id",function(req,res){
  Entry.findById(req.params.id,function (err,todo) {
  if (err) {
    console.log(err);
  }else{
    console.log("rtt",entry);
    res.render("edit/editPage",{entry:entry})
  }
  })
})
Title.find({},function (err, titleCollection) {
      if(err) {
           console.log(err);
      } else {
          titleCollection.forEach(item=> {
                if(err) {
                     console.log(err)
                } else {
                    router.get(`/${item.contentTitle.replace(/\s/g, "-")}`, (req, res) => {
                      req.session.like = false
                      // eğer tersten sıralamak istersen sort({$natural:-1}).then(entryCollection)
                      Entry.find({entryTitle:item.contentTitle}, function(err, entryCollection) {
                          res.render("entry/entryPage", {title: item.contentTitle,entry:entryCollection,user:req.user,titleOpen:item.openTitleUser})
                        });
                    }); 
                }  
          })
        
      }
  });
Title.find({}).then((result) => {
  result.forEach((item) => {
    router.post(`/${item.contentTitle}`, (req, res) => {
      var entry = item.contentTitle;
      entry = new Entry({
        entry: req.body.entry,
        entryTitle: req.body.entryTitle,
        postNick:req.body.postNick,
        date: new Date().toLocaleDateString(),
        time: new Date().toLocaleTimeString(),
        http: req.body.http,
        httpName: req.body.httpName
      })
      entry
        .save()
        .then((results) => {
          req.session.flashMessage = {
            content : "entry mükemmle bir şekilde gönderildi..."
          }
          console.log(results);
          res.redirect(`/entry/${item.contentTitle.replace(/\s/g, "-")}`);
        })
        .catch((err) => {
          console.log(err);
        });
    });
  });
});

// Entry.findOne({}).populate('postedBy').exec(function(err,person){
//   if (err) {
//     console.log(err);
//   } else {
//     console.log("person",person);
//   }
// })

//delete
// router.get("/sil/:text", (req, res) => {
//   Entry.findOneAndDelete({entry:req.params.entry},function (err, results) {
//     if (err) {
//       console.log("hata var"+err);
//     }
//     else{
//       Title.find({}).then(result=> {
//         result.forEach(item=> {
//             res.redirect(`/entry/${item.contentTitle.replace(/\s/g , "-")}`)
//         })
//       })
//     }
//   });
// });
module.exports = router;
