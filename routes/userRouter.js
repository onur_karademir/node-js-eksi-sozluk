const express = require("express");
const router = express.Router();
const bcrypt = require('bcryptjs');
const password =require("passport");
const User = require("../models/userModel");

router.get("/", (req, res) => {
  res.render("user/register");
});
router.post("/", (req, res) => {
  const { nick, email, password } = req.body;
  let errorMessage = [];
  if (!nick || !email || !password) {
    errorMessage.push({ msg: "Tüm alanları doldurmalısınız..." });
  }
  if (password.length < 6) {
    errorMessage.push({ msg: "Şifre an az 6 karakter olmalıdır..." });
  }
  if (errorMessage.length > 0) {
    res.render("user/register", {
        errorMessage,
        nick,
        email,
        password,
    });
  }else {
      User.findOne({ email: email }).then((user)=>{
          console.log(user);
          if(user && nick){
            errorMessage.push({ msg: "Kullanıcı çoktan atı almış Üsküdar' ı geçmiş..." });
            res.render("user/register", {
                errorMessage,
                nick,
                email,
                password,
            });
          }else{
              var user = new User({
                  nick,
                  email,
                  password
              })
              bcrypt.genSalt(10, (err, salt) => {
                bcrypt.hash(user.password, salt, (err, hash) => {
                    if (err) throw err;
                    user.password = hash;
                    user.save().then(user => {
                        res.redirect('user/login');
                        console.log(hash);
                    })
                      .catch(err => console.log(err));
                  });
              })
          }
      })
  }
});
router.post("/login", (req, res, next) => {
  password.authenticate("local", {
    successRedirect: "/user/dashboard",
    failureRedirect: "/",
  })(req, res, next);
});
module.exports = router;
