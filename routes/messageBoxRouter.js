const express = require("express");
const router = express.Router();
const User = require("../models/userModel");


router.get("/:userWho",function(req,res){
    const {nick} = req.user;
    User.find({nick:nick}).then(result=> {
        console.log("mesaj kutusu:",result);
        res.render(`message/messageBox`,{collection:result})
    })
})
router.get("/:userWho/sendbox",function(req,res){
    const {nick} = req.user;
    User.find({nick:nick}).then(result=> {
        res.render(`message/sendMessageBox`,{collection:result})
    })
})
router.get("/:userWho/inbox",function(req,res){
    const {nick} = req.user;
    User.find({nick:nick}).then(result=> {
        res.render(`message/inMessageBox`,{collection:result})
    })
})





module.exports = router;