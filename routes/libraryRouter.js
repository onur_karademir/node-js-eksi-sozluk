const express = require("express");
const router = express.Router();
const Entry = require("../models/entryModel");
const User = require("../models/userModel");

router.get("/:user/detail",(req,res)=> {
    const {nick} = req.user;
    var filter = req.params.user
    User.find({nick:nick}).then(result=> {
        res.render("user/libraryDetail",{user:result,title:filter})
    })
});
router.get("/:title",(req,res)=> {
    const {nick} = req.user;
    const title = req.params.title;
    User.find({nick:nick}).then(result=> {
        result.forEach(item=> {
            var arr = item.libraryStore
            var unique = arr.filter(function (a) {
                return !this[a.entryTitle] && (this[a.entryTitle] = true);
            }, Object.create(null));
            res.render("user/library",{user:unique})

        })
    })
});
//delete library//
router.get("/sil/:title",(req, res) => {
    const {nick} = req.user;
    req.session.flashMessage = {
        content : "Başlık kitaplıktan kaldırıldı..."
    }
    User.updateOne(
        {nick: nick},
        {$pull: {libraryStore: {entryTitle: req.params.title}}},
        function(err, result) {
            if (err) {
                console.log(err);
            } else {
                res.redirect(`/library/${nick.replace(/\s/g,"-")}`)
            }
        }
    )
});
//delete library detail//
router.get("/sil/detail/:title/:entry",(req, res) => {
    const {nick} = req.user;
    var libEntry = req.params.entry
    var libTitle = req.params.title
    req.session.flashMessage = {
        content : "Entry kitaplık detayından kaldırıldı, kendisi çok üzgün..."
    }
    User.updateOne(
        {nick: nick},
        {$pull: {libraryStore: {entry: libEntry}}},
        function(err, result) {
            if (err) {
                console.log(err);
            } else {
                res.redirect(`/library/${libTitle.replace(/\s/g,"-")}/detail`)
            }
        }
    )
});
router.post("/:id",function(req,res){
    const {nick} = req.user;
    const userId = req.user._id;
    console.log("userId",userId);
    const libraryId = req.params.id;
    console.log(libraryId);
    console.log(userId);
    console.log("nick",nick);
    User.findById(userId).then(async (user)=> {
        console.log("user",user);
        var bookArray = await user.libraryStore
        var findLibrary = await bookArray.some(book=> {
            console.log("libstore",book.id);
            console.log("libstore",libraryId);
            return book.id == libraryId
        })
        console.log("libstore",findLibrary);
        if (!findLibrary) {
            req.session.flashMessage = {
                content : "entry kitaplığa gönderldi..."
            }
            Entry.findOne({_id:libraryId}).then(entry=> {
                var libraryObj = {
                    id:entry._id,
                    entry:entry.entry,
                    postNick:entry.postNick,
                    entryTitle:entry.entryTitle,
                    date:entry.date,
                    time:entry.time,
                    editFlag:entry.editFlag,
                    editTime:entry.editTime,
                    editDate:entry.editDate,
                    http:entry.http,
                    httpName:entry.httpName,
                }
                console.log("libraryId",entry);
                User.findByIdAndUpdate(userId,{$push:{libraryStore:libraryObj}},(err)=> {
                    if (err) {
                        console.log(err);
                    }
                })
            })
        }else{
            req.session.flashMessage = {
                content : "entry zaten kitaplığında mevcut..."
            }
        }
    })
    Entry.findById(libraryId).exec().then(result=> {
        console.log("asd",result);
        res.redirect(`/entry/${result.entryTitle.replace(/\s/g , "-")}`)
    })
})



module.exports = router;