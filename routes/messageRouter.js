const express = require("express");

const router = express.Router();

const User = require("../models/userModel");

User.find({}).then(result=> {
    result.forEach(user=> {
        router.get(`/${user.nick.replace(/\s/g ,"-")}`, (req, res) => {
            const {nick} = req.user
            const connectedUser = user.nick;
            console.log("kime gidiyor:",connectedUser);
            res.render("message/messageSend",{nick:connectedUser,whoLogin:nick,userData:user})
        });
    });
});

router.post("/:user",(req,res)=> {
    const whoMessage = req.params.user;
    const {nick} = req.user;
    const me = req.user.nick;
    const {message} = req.body;
    var messageObj = {message:req.body.message,whoSend:nick,mDate:new Date().toLocaleDateString(),mTime:new Date().toLocaleTimeString()};
    
    var messageSend = {sendMessage:req.body.message,toWho:whoMessage,toWhoMdate:new Date().toLocaleDateString(),toWhoMtime:new Date().toLocaleTimeString()};

    //var msgObj = {gelen:[{msgArray:[{msg:req.body.message,msgGonderen:nick}],msgGonderen:nick}]};

    console.log("kime gönderildi:",whoMessage);
    User.findOneAndUpdate({nick:whoMessage},{$push:{messageBox:messageObj}},(err)=> {
        if (err) {
         console.log(err);
        }else{
            User.findOneAndUpdate({nick:me},{$push:{messageSendBox:messageSend}},(err)=> {
                if (err) {
                    console.log(err);
                }else{
                    res.redirect(`/message/${whoMessage.replace(/\s/g,"-")}`)
                }
            })
        };
    });
});

router.get("/sil/:_id",(req, res) => {
    console.log("id:",req.params._id);
    const {nick} = req.user;
    User.update(
        {nick: nick},
        {$pull: {messageBox: {_id: req.params._id}}},
        function(err, result) {
            if (err) {
                console.log(err);
            } else {
                res.redirect(`/messagebox/${nick.replace(/\s/g,"-")}/inbox`)
            }
        }
    )
});
router.get("/sendbox/sil/:_id",(req, res) => {
    console.log("id:",req.params._id);
    const {nick} = req.user;
    User.update(
        {nick: nick},
        {$pull: {messageSendBox: {_id: req.params._id}}},
        function(err, result) {
            if (err) {
                console.log(err);
            } else {
                res.redirect(`/messagebox/${nick.replace(/\s/g,"-")}/sendbox`)
            }
        }
    )
});

module.exports = router;
