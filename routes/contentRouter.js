const express = require("express");
const router = express.Router();
const Title = require("../models/titleModel");

router.get("/new-title",(req,res)=> {
    res.render("title/content")
})
router.post("/new-title",async (req,res)=> {
    String.prototype.turkishtoEnglish = function () {
        return this.replace('Ğ','g')
            .replace('Ü','u')
            .replace('Ş','s')
            .replace('I','i')
            .replace('İ','i')
            .replace('Ö','o')
            .replace('Ç','c')
            .replace('ğ','g')
             .replace('ü','u')
            .replace('ş','s')
            .replace('ı','i')
            .replace('ö','o')
            .replace(/\s/g , "-")
            .replace('ç','c');
    };
    var titleError = [];
    var titleFlag = false;
    const {contentTitle} = req.body
    var a = contentTitle.turkishtoEnglish()
    console.log("gönd baslik",a);
    var title = await Title.find({contentTitle:a});
    console.log("title",title);
    title.forEach(item=> {
        if (item.contentTitle === a) {
          titleFlag = true;
          console.log("flag",titleFlag);  
        }
    })
    if (titleFlag == true) {
        titleError.push({msg:"o başlık bizde mevcut, yeni bir şeyler denemelsin..."})
    }
    if (contentTitle === "") {
        titleError.push({msg:"başlık açmak için en az bir tuşa basmayı denemelisin, öyle daha iyi olur bence..."})
    }
    if (contentTitle.length > 25) {
        titleError.push({msg:"o kadar uzun başlık olmaz kardeş..."})
    }
    if (titleError.length > 0) {
        res.render("title/content",{titleError})        
    }
    else{
        var title = await new Title({
            contentTitle: req.body.contentTitle.turkishtoEnglish(),
            openTitleUser: req.body.openTitleUser
        })
        await title.save().then(results=> {
            res.redirect("/")
        }).catch(err=> {
            console.log(err);
            res.send(err)
        })
    }
   
})

module.exports = router;