const express = require("express");

const router = express.Router();
const Todo = require("../models/postModel");
const Title = require("../models/titleModel");
const Entry = require("../models/entryModel");
const User = require("../models/userModel");

router.get("/",(req,res)=> {
  const postPerPage = 15;
  const page = req.query.page || 1
  Entry.find({}, function(err, result) {
    if(err) {
         console.log(err);
    } else {
         Title.find({})
           .skip((postPerPage * page) - postPerPage)
           .limit(postPerPage).then(collection2=>{
            Title.countDocuments().then(count=>{
              if(err) {
                   console.log(err)
              } else {
                if (req.query.name != null && req.query.name !== '') {
                  const userQuery = new RegExp(req.query.name, 'i')
                  var titleQuery= new RegExp(req.query.name,'i');
                  User.find({nick:userQuery}).then(user=> {
                    Title.find({contentTitle:titleQuery}).then(title=> {
                      res.render("index", {todos: result, title: collection2,titleQuery:title,userQuery:user,current:parseInt(page),pages:Math.ceil(count/postPerPage)});
                    })
                    console.log("user query",user);
                  })
                }
                else{
                    var titleLength = result.filter(j => {
                      return j.entryTitle === "kelime"
                    })
                  res.render("index", {todos: result, title: collection2,userQuery:null,titleQuery:null,current:parseInt(page),pages:Math.ceil(count/postPerPage)});
                }
              }  

            })
         }); 
    }
});

})
router.get("/sil/:text",(req, res) => {
  Entry.find({entry:req.params.text}).then(result=> {
    req.session.flashMessage = {
      content : "Entry silindi, süpürüldü yerinde yeller esiyor..."
    }
   result.forEach(item => {
    res.redirect(`/entry/${item.entryTitle.replace(/\s/g , "-")}`)
   });
  })
  Entry.findOneAndDelete({entry:req.params.text},function (err, results) {
    if (err) {
      console.log("hata var"+err);
    }
  });
});
router.post("/:id/completed",(req,res)=> {
  const {nick} = req.user;
  var likeObj = {likeNick:nick,likeFlag:true}
  let entryID = req.params.id;
  Entry.findById(entryID).exec().then(result=> {
   console.log("res",result.likeArray);
   var like = result.likeArray
   var findUser = like.some(findUser=> {
      return nick === findUser.likeNick
   });
   console.log("find:",findUser);
   if (findUser) {
     req.session.flashMessage= {
       content:"Beğenmekten vazgeçtiniz, göz yaşları sel oldu akıyor..."
     }
    Entry.update(
      {_id: entryID},
      {$pull: {likeArray: {likeNick: nick}}},
      function(err, result) {
          if (err) {
              console.log(err);
          } else {
            Entry.findById(entryID).exec().then(result=> {
              res.redirect(`/entry/${result.entryTitle.replace(/\s/g , "-")}`)
            })
          }
      }
    )
   }else{
      Entry.findByIdAndUpdate(entryID,{$push:{likeArray:likeObj}},(err)=> {
        req.session.flashMessage= {
          content:"Entry beğenildi, kendisi çok hoşnut..."
        }
    if (err) {
      console.log(err);
    }else{
    Entry.findById(entryID).exec().then(result=> {
    res.redirect(`/entry/${result.entryTitle.replace(/\s/g , "-")}`)
    })
    }
  })
   }
  })
  //eski sistemim//
  // Entry.findByIdAndUpdate(entryID,{$push:{likeArray:likeObj}},(err)=> {
  //   if (err) {
  //     console.log(err);
  //   }else{
  //   Entry.findById(entryID).exec().then(result=> {
  //     console.log("result:",result);
  //   result.like = ! result.like;
  //   res.redirect(`/entry/${result.entryTitle.replace(/\s/g , "-")}`)
  //   return result.save()
  // })
  //   }
  // })
 
})
router.get("/edit/:id",function(req,res){
  Entry.findById(req.params.id,function (err,entry) {
  if (err) {
    console.log(err);
  }else{
    res.render("edit/editPage",{entry:entry})
  }
  })
})
router.post("/comment/:_id", function(req,res){
  const {nick}= req.user
  var objComment = {comment:req.body.comment,commentNick:nick,commentDate:new Date().toLocaleDateString(),commentTime:new Date().toLocaleTimeString()};
Entry.findByIdAndUpdate(req.params._id,{$push: {commentArray: objComment}},(err)=> {
  if (err) {
    console.log(err);
  }else{
    let entryID = req.params._id
    Entry.findById(entryID).exec().then(result=> {
      res.redirect(`/entry/${result.entryTitle.replace(/\s/g , "-")}`)
    })
  }
})
});
router.post("/edit/:_id", function(req,res){
  Entry.findByIdAndUpdate(req.params._id,{entry: req.body.entry, editFlag: true, editTime:new Date().toLocaleTimeString(),editDate: new Date().toLocaleDateString() },(err)=>{
   if (err) {
     console.log(err);
   } else {
    let entryID = req.params._id
    Entry.findById(entryID).exec().then(result=> {
      req.session.flashMessage = {
        content : "Entry editlendi, paketlendi ve yerine yerleştirildi..."
      }
      console.log("asd",result);
      res.redirect(`/entry/${result.entryTitle.replace(/\s/g , "-")}`)
    })
   }
 })
})
//today page get
router.get("/today",(req,res)=> {
  Title.find({}, function(err, collection2) {
    if(err) {
         console.log(err)
    } else {
         res.render("today/today", {title: collection2});
    }  
}); 
})

module.exports = router;