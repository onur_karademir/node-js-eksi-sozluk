const express = require("express");
const password =require("passport");
const router = express.Router();

router.get("/login", (req, res) => {
  res.render("user/login");
});
router.post("/login", (req, res, next) => {
  password.authenticate("local", {
    successRedirect: "/",
    failureRedirect: "/user/login",
  })(req, res, next);
});

module.exports = router;
