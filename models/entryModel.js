const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const Title = require("./titleModel");

var entrySchema = new Schema({
  entry: {
    type: String,
    require: true,
    trim: true,
  },
  postNick: {
    type: String,
    required: true,
  },
  like: {
    type: Boolean,
    default: false,
  },
  likeCount: {
    type: String,
    default: "0",
  },
  entryTitle: {
    type: String,
    required: true,
  },
  date: {
    type: String,
  },
  time: {
    type: String,
  },
  editFlag: {
    type: Boolean,
    default: false,
  },
  editTime: {
    type: String,
  },
  editDate: {
    type: String,
  },
  http: {
    type: String,
    default: null
  },
  httpName: {
    type: String,
    default: null
  },
  commentArray:[{
    comment:String,
    commentNick:String,
    commentDate:String,
    commentTime:String
  }],
  likeArray:[{
    likeNick:String,
    likeFlag:false
  }]
});
var Entry = mongoose.model("Entry", entrySchema);

module.exports = Entry;
