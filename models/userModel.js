const mongoose = require("mongoose");
const Schema = mongoose.Schema;


var newUser = new Schema({
    nick:{
        type:String,
        required:true,
        trim:true
    },
    email:{
        type:String,
        required:true,
        trim:true
    },
    password:{
        type:String,
        required:true,
        trim:true
    },
    messageBox:[{
        whoSend:String,
        message:String,
        mDate:String,
        mTime:String,
    }],
    messageSendBox:[{
        toWho:String,
        sendMessage:String,
        toWhoMdate:String,
        toWhoMtime:String,
    }],
    profileImg:{
        type:String
    },
    libraryStore:[
        // {
        //     id:String,
        //     entry:String,
        //     postNick:String,
        //     entryTitle:String,
        //     date:String,
        //     time:String,
        //     editFlag:String,
        //     editTime:String,
        //     editDate:String,
        //     http:String,
        //     httpName:String,
        // }
    ]
})

var User = mongoose.model("User",newUser);

module.exports = User;

