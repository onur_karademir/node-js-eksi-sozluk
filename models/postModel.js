const mongoose = require("mongoose");
const Schema = mongoose.Schema;

var todoSchema = new Schema({
  title: {
    type: String,
    require: true,
    trim:true
  },
  todo: {
    type: String,
    require: true,
    trim:true
  },
  postNick: {
    type: String,
    required: true,
  },
  like: {
    type: Boolean,
    default: false,
  },
});

var Todo = mongoose.model("Todo", todoSchema);

module.exports = Todo;
