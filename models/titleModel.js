const mondoose = require("mongoose");

const Schema = mondoose.Schema;

var newTitle = new Schema({
  contentTitle: {
    type: String,
    required: true,
    trim:true
  },
  openTitleUser:{
    type:String,
    required:true,
    trim:true
  }
});

var Title = mondoose.model("Title",newTitle);

module.exports = Title;