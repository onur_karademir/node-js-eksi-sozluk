//lib call
const fileUpload = require("express-fileupload")
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const path = require("path");
const express = require("express");
const app = express();
const expressLayouts = require("express-ejs-layouts");
const passport = require("passport");
const flash = require("connect-flash");
const session = require("express-session")
const cookieParser = require('cookie-parser');
require("./config/passport")(passport);
//router call
const indexRouter = require("./routes/indexRouter");
const userRouter = require("./routes/userRouter");
const loginRouter = require("./routes/loginRouter");
const logoutRouter = require("./routes/logoutRouter");
const dashboardRouter = require("./routes/dashboardRouter");
const postRouter = require("./routes/postRouter");
const contentRouter = require("./routes/contentRouter");
const entryRouter = require("./routes/entryRouter");
const userPageRouter = require("./routes/userPageRouter");
const messageRouter = require("./routes/messageRouter");
const messageBoxRouter = require("./routes/messageBoxRouter");
const libraryRouter = require("./routes/libraryRouter");
const Todo = require("./models/postModel");
const Title = require("./models/titleModel");
const Entry = require("./models/entryModel");
const User = require("./models/userModel");

//mongosse connected //
mongoose
  .connect(
    "mongodb+srv://aidenpearce:Onur050263007@cluster0.bvq9p.mongodb.net/<dbname>?retryWrites=true&w=majority",
    { useNewUrlParser: true, useUnifiedTopology: true }
  )
  .then((db) => console.log("db is connected database..."))
  .catch((error) => console.log(error));
//app set
app.set("view engine", "ejs");
//app use
app.use(cookieParser());
app.use(expressLayouts);
app.use("/public", express.static(path.join(__dirname, "public")));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(fileUpload());
app.use(flash());
app.use(
  session({
    secret: "secret",
    resave: true,
    saveUninitialized: true,
  })
);
app.use(passport.initialize());
app.use(passport.session());
//app router use
app.use("/", (req, res, next) => {
  res.locals.loggedIn = req.isAuthenticated();
  next();
});
app.use("/", (req, res, next) => {
  res.locals.userName = { user: req.user }
  next();
});
app.use((req, res, next) => {
  res.locals.flashMessage = req.session.flashMessage
  delete req.session.flashMessage
  next()
});
app.use((req, res, next) => {
  res.locals.like = req.session.like
  next()
});
app.use("/", indexRouter);
app.use("/new-user", userRouter);
app.use("/user", loginRouter);
app.use("/user", dashboardRouter);
app.use("/logout", logoutRouter);
app.use("/post", postRouter);
app.use("/post", contentRouter);
app.use("/entry", entryRouter);
app.use("/pages", userPageRouter);
app.use("/message", messageRouter);
app.use("/messagebox", messageBoxRouter);
app.use("/library", libraryRouter);


/////////////////////////////////////
app.listen(process.env.PORT || 5000);

console.log("local host://5000 ===>>> app start...")
